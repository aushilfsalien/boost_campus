<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme Boost Campus Login - Layout file.
 *
 * @package   theme_boost_campus
 * @copyright 2017 Kathrin Osswald, Ulm University kathrin.osswald@uni-ulm.de
 * @copyright based on code from theme_boost by Damyon Wiese
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;

$bodyattributes = $OUTPUT->body_attributes();
// TU DORTMUND MODIFICATION START
// SSO LOGIN
$ssotitle = '<h4>Login mit UniAccount</h4>';
$ssologinform = '<p style="margin-top: 3rem;"><input class="btn btn-secondary" onclick="window.location.href=\'https://sso.itmc.tu-dortmund.de/openam/UI/Login?goto=http://' . $_SERVER['HTTP_HOST'] . '/login\'"  value="Login mit UniAccount" type="button"></p><p style="margin-top: 20px;">Sicheres Login durch:</p>';
$ssologinhtml = '<p><a href="http://www.itmc.tu-dortmund.de/sso"><img class="center-block" src="/theme/boost_campus/pix/Logo_sso_66x29.png" alt="single sign on/off" role="presentation" style="vertical-align:text-bottom; margin: 0 .5em;" class="img-responsive" height="29" width="66"></a></p>';

// UARUHR LOGIN
$uaruhrtitle = '<h4>UA Ruhr Login</h4>';
$uaruhrloginhtml = '<p><a href="/auth/shibboleth/login.php"><img class="center-block" src="/theme/boost_campus/pix/UARuhrLoginLogo.png" style="margin-top: 2em; width: 180px; max-width: 200px"></a></p><br /><p>Um zum UA Ruhr Login zu gelangen bitte auf das Bild klicken.</p>';

// STANDARD LOGIN
$standardtitle = '<h4>Login ohne UniAccount</h4>';
$forgotpasswordurl = '/login/forgot_password.php';
$loginurl = get_login_url();
$cookieshelpiconformatted = $OUTPUT->help_icon('cookiesenabled');

// AUTH INSTRUCTIONS
// check if auth instructions are filled in the config if not do not display block
$instructions = $CFG->auth_instructions;
if(!empty($instructions))
{
    $hasinstructions = true;
}
else
{
    $hasinstructions = false;
}

// TU DORTMUND MODIFICATION END

$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'bodyattributes' => $bodyattributes,
    // TU DORTMUND MODIFICATION START
    'ssotitle' => $ssotitle,
    'ssologinform' => $ssologinform,
    'ssologinhtml' => $ssologinhtml,
    'uaruhrtitle' => $uaruhrtitle,
    'uaruhrloginhtml' => $uaruhrloginhtml,
    'standardtitle' => $standardtitle,
    'forgotpasswordurl' => $forgotpasswordurl,
    'loginurl' => $loginurl,
    'cookieshelpiconformatted' => $cookieshelpiconformatted,
    'instructions' => $instructions,
    'hasinstructions' => $hasinstructions
    // TU DORTMUND MODIFICATION END
];

echo $OUTPUT->render_from_template('theme_boost_campus/login', $templatecontext);

// MOFIFICATION START.
// Include own layout file for the footnote region.
// The theme_boost/login template already renders the standard footer.
// The footer blocks and the image area are currently not shown on the login page.
// Here, we will add the footnote only.
// Get footnote config.
$footnote = get_config('theme_boost_campus', 'footnote');
if (!empty($footnote)) {
    // Add footnote layout file.
    require_once(__DIR__ . '/includes/footnote.php');
}
// MODIFICATION END.
